/**
 * Created by jasonc on 7/26/16.
 */

var app = angular.module('practichemProject', ['ngMessages']);

app.controller('mainController', ['$scope', function($scope) {

    // Array that contains all validated users
    $scope.users = [];

    // Each ng-model is bounded to a property name
    $scope.submitUser = function() {
        var userNameRegEx = /^[a-zA-Z]{5,15}$/; // 5-15 characters
        var emailRegEx = /^\S+@\S+\.\S+$/; // email address format

        // Will not let the user submit if it doesn't match the regular expressions
        if ($scope.userName.match(userNameRegEx) && $scope.emailAddress.match(emailRegEx)) {
            $scope.users.push({
                name: $scope.fullName,
                username: $scope.userName,
                email: $scope.emailAddress
            });

            $scope.fullName = '';
            $scope.userName = '';
            $scope.emailAddress = '';
        }

        else {
            // Generic alert (bad practice but enough for this example)
            alert("Please enter correct username length or email format.");
        }


    };

    // Deletes user on click action
    $scope.deleteUser = function(index) {
        $scope.users.splice(index, 1)
    };

}]);

//*** Directives for cleaner pages ***//
app.directive('userForm', function() {
   return {
       restrict: 'AE',
       templateUrl: 'templates/userform.html'
   };
});

app.directive('userTable', function() {
    return {
        restrict: 'AE',
        templateUrl: 'templates/usertable.html'
    };
});