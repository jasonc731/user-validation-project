User validation project using AngularJS and ngMessages for Practichem.
Checks for:
1. Name
    - This is required.
2. Username
    - This is required.
    - Minimum length is 5 characters.
    - Maximum length is 15 characters.
3. Email
    - This is required.
    - Must be in the proper format (example@email.com).
    
If the fields are properly filled out, only then will you be able to add users
to the table.